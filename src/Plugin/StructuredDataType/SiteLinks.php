<?php

namespace Drupal\structured_data\Plugin\StructuredDataType;

use Drupal\structured_data\StructuredDataTypeBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'site links' structured data type.
 *
 * @StructuredDataType(
 *   id = "site_links",
 *   name = @Translation("Site Links"),
 *   type = "WebSite"
 * )
 */
class SiteLinks extends StructuredDataTypeBase {
  
  public function getData() {
    return parent::getData() + $this->getSiteData();
  }

  protected function getSiteData() {
    return [
      'potentialAction' => $this->getActions(),
    ];
  }

  protected function getActions() {
    return [
      '@type' => 'SearchAction',
      'target' => \Drupal::request()->getSchemeAndHttpHost() . \Drupal::config('structured_data.settings')->get('site_links.search_target') . '{search_term_string}',
      'query-input' => 'required name=search_term_string',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state, $config) {
    $build = [];

    $build['site_links__search_target'] = [
      '#title' => t('Search results url'),
      '#type' => 'textfield',
      '#default_value' => !is_null($config) ? $config->get('site_links.search_target') : $this->defaultConfiguration()['search_target'],
      '#description' => t("The search results url including the query parameters"),
    ];

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'search_target' => 'search?search=',
    ];
  }

}
