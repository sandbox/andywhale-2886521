<?php

namespace Drupal\structured_data\Plugin\StructuredDataType;

use Drupal\Core\Link;
use Drupal\structured_data\StructuredDataTypeBase;
use Drupal\structured_data\Exception\StructuredDataException;

/**
 * Provides a 'breadcrumb' structured data type.
 *
 * @StructuredDataType(
 *   id = "breadcrumb",
 *   name = @Translation("Breadcrumb"),
 *   type = "BreadcrumbList"
 * )
 */
class Breadcrumb extends StructuredDataTypeBase {
  
  public function getData() {
    $breadcrumbData = $this->getBreadcrumbData();
    if ($breadcrumbData) {
      return parent::getData() + $breadcrumbData;
    }
    return [];
  }

  protected function getBreadcrumbData() {
    $breadcrumbData = \Drupal::Service('breadcrumb')->build(\Drupal::routeMatch());
    if (!$breadcrumbData->getLinks()) {
      throw new StructuredDataException('Content item does not have a breadcrumb');
    }
    $data = $this->getBreadcrumbBaseArray();
    $position = 1;
    foreach ($breadcrumbData->getLinks() as $breadcrumbItem) {
      $data['itemListElement'][] = $this->getBreadcrumbItem($breadcrumbItem, $position);
      $position++;
    }
    return $data;
  }

  protected function getBreadcrumbBaseArray() {
    return [
      "itemListElement" => [],
    ];
  }

  protected function getBreadcrumbItem(Link $breadcrumbLink, $position) {
    return [
      "@type" => "ListItem",
      "position" => $position,
      "item" => [
        "@id" => $breadcrumbLink->getUrl()->setAbsolute()->toString(),
        "name" => $breadcrumbLink->getText(),
      ]
    ];
  }

}