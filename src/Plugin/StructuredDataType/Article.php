<?php

namespace Drupal\structured_data\Plugin\StructuredDataType;

use Drupal\structured_data\EntityStructuredDataTypeBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'site links' structured data type.
 *
 * @StructuredDataType(
 *   id = "article",
 *   name = @Translation("Article"),
 *   type = "NewsArticle"
 * )
 */
class Article extends EntityStructuredDataTypeBase {
  
  public function getData() {
    return parent::getData() + $this->getEventData();
  }

  protected function getEventData() {
    $config = \Drupal::config('structured_data.settings');
    return [
      'mainEntityOfPage' => [
        '@type' => 'WebPage',
        '@id' => \Drupal::request()->getUri(),
      ],
      'headline' => $this->tokenizeString($config->get('article.headline')),
      'image' => [
        '@type' => 'ImageObject',
        'url' => $this->tokenizeString($config->get('article.image_url')),
        'height' => $this->tokenizeString($config->get('article.image_height')),
        'width' => $this->tokenizeString($config->get('article.image_width')),
      ],
      'datePublished' => $this->tokenizeString($config->get('article.published')),
      'dateModified' => $this->tokenizeString($config->get('article.modified')),
      'description' => $this->tokenizeString($config->get('article.description')),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state, $config) {
    $build = parent::buildConfigurationForm($form, $form_state, $config);

    $token_types = ['node'];
    
    $build['article__headline'] = [
      '#title' => t('Headline'),
      '#type' => 'textfield',
      '#default_value' => !is_null($config) ? $config->get('article.headline') : $this->defaultConfiguration()['headline'],
      '#description' => t("The string to be used for the article healdine (this can include tokens)"),
      '#element_validate' => array('token_element_validate'),
      '#after_build' => array('token_element_validate'),
      '#token_types' => $token_types,
    ];

    $build['article__image_url'] = [
      '#title' => t('Image URL'),
      '#type' => 'textfield',
      '#default_value' => !is_null($config) ? $config->get('article.image_url') : $this->defaultConfiguration()['image_url'],
      '#description' => t("The string to be used for the article image URL (this can include tokens)"),
      '#element_validate' => array('token_element_validate'),
      '#after_build' => array('token_element_validate'),
      '#token_types' => $token_types,
    ];

    $build['article__image_height'] = [
      '#title' => t('Image height'),
      '#type' => 'textfield',
      '#default_value' => !is_null($config) ? $config->get('article.image_height') : $this->defaultConfiguration()['image_height'],
      '#description' => t("The string to be used for the article image height (this can include tokens)"),
      '#element_validate' => array('token_element_validate'),
      '#after_build' => array('token_element_validate'),
      '#token_types' => $token_types,
    ];

    $build['article__image_width'] = [
      '#title' => t('Image width'),
      '#type' => 'textfield',
      '#default_value' => !is_null($config) ? $config->get('article.image_width') : $this->defaultConfiguration()['image_width'],
      '#description' => t("The string to be used for the article image width (this can include tokens)"),
      '#element_validate' => array('token_element_validate'),
      '#after_build' => array('token_element_validate'),
      '#token_types' => $token_types,
    ];

    $build['article__published'] = [
      '#title' => t('Article published'),
      '#type' => 'textfield',
      '#default_value' => !is_null($config) ? $config->get('article.published') : $this->defaultConfiguration()['published'],
      '#description' => t("The string to be used for the article published date (this can include tokens)"),
      '#element_validate' => array('token_element_validate'),
      '#after_build' => array('token_element_validate'),
      '#token_types' => $token_types,
    ];

    $build['article__modified'] = [
      '#title' => t('Article modified'),
      '#type' => 'textfield',
      '#default_value' => !is_null($config) ? $config->get('article.modified') : $this->defaultConfiguration()['modified'],
      '#description' => t("The string to be used for the article modified date (this can include tokens)"),
      '#element_validate' => array('token_element_validate'),
      '#after_build' => array('token_element_validate'),
      '#token_types' => $token_types,
    ];

    $build['article__description'] = [
      '#title' => t('Article description'),
      '#type' => 'textfield',
      '#default_value' => !is_null($config) ? $config->get('article.description') : $this->defaultConfiguration()['description'],
      '#description' => t("The string to be used for the article description (this can include tokens)"),
      '#element_validate' => array('token_element_validate'),
      '#after_build' => array('token_element_validate'),
      '#token_types' => $token_types,
    ];

    $build['article__token_help'] = array(
      '#theme' => 'token_tree_link',
      '#token_types' => $token_types,
    );

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'type' => [],
      'headline' => '[node:title]',
      'image_url' => '',
      'image_height' => '',
      'image_width' => '',
      'published' => '[node:created:html_datetime]',
      'modified' => '[node:changed:html_datetime]',
      'description' => '',
    ];
  }

}
