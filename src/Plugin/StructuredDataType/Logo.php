<?php

namespace Drupal\structured_data\Plugin\StructuredDataType;

use Drupal\structured_data\StructuredDataTypeBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'logo' structured data type.
 *
 * @StructuredDataType(
 *   id = "logo",
 *   name = @Translation("Logo"),
 *   type = "Organization"
 * )
 */
class Logo extends StructuredDataTypeBase {
  
  public function getData() {
    return parent::getData() + $this->getLogoData();
  }

  protected function getLogoData() {
    return [
      'url' => \Drupal::request()->getSchemeAndHttpHost(),
      'logo' => \Drupal::request()->getSchemeAndHttpHost() . \Drupal::config('structured_data.settings')->get('logo.url'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state, $config) {
    $build = [];

    $build['logo__url'] = [
      '#title' => t('Logo url'),
      '#type' => 'textfield',
      '#default_value' => !is_null($config) ? $config->get('logo.url') : $this->defaultConfiguration()['url'],
      '#description' => t("The location of your logo file starting with a slash (excluding site URL) - e.g. /themes/logo.png"),
    ];

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'url' => '',
    ];
  }

}
