<?php

namespace Drupal\structured_data\Plugin\StructuredDataType;

use Drupal\structured_data\StructuredDataTypeBase;

/**
 * Provides a 'site name' structured data type.
 *
 * @StructuredDataType(
 *   id = "site_name",
 *   name = @Translation("Site Name"),
 *   type = "WebSite"
 * )
 */
class SiteName extends StructuredDataTypeBase {
  
  public function getData() {
    return parent::getData() + $this->getSiteData();
  }

  protected function getSiteData() {
    return [
      'url' => \Drupal::request()->getSchemeAndHttpHost(),
      'name' => \Drupal::config('system.site')->get('name'),
    ];
  }

}
