<?php

namespace Drupal\structured_data\Plugin\StructuredDataType;

use Drupal\structured_data\EntityStructuredDataTypeBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'site links' structured data type.
 *
 * @StructuredDataType(
 *   id = "event",
 *   name = @Translation("Event"),
 *   type = "Event"
 * )
 */
class Event extends EntityStructuredDataTypeBase {
  
  public function getData() {
    return parent::getData() + $this->getEventData();
  }

  protected function getEventData() {
    $config = \Drupal::config('structured_data.settings');
    return [
      'name' => $this->tokenizeString($config->get('event.name')),
      'url' => \Drupal::request()->getUri(),
      'startdate' => $this->tokenizeString($config->get('event.startdate')),
      'enddate' => $this->tokenizeString($config->get('event.enddate')),
      'description' => $this->tokenizeString($config->get('event.description')),
      'image' => $this->tokenizeString($config->get('event.image_url')),
      'offers' => ($this->tokenizeString($config->get('event.offers'))) ?: \Drupal::request()->getUri(),
      'location' => [
        '@type' => 'EventVenue',
        'name' => $this->tokenizeString($config->get('event.location')),
        'address' => $this->tokenizeString($config->get('event.address')),
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state, $config) {
    $build = parent::buildConfigurationForm($form, $form_state, $config);

    $token_types = ['node'];
    
    $build['event__name'] = [
      '#title' => t('Event name'),
      '#type' => 'textfield',
      '#default_value' => !is_null($config) ? $config->get('event.name') : $this->defaultConfiguration()['name'],
      '#description' => t("The string to be used for the event (this can include tokens)"),
      '#element_validate' => array('token_element_validate'),
      '#after_build' => array('token_element_validate'),
      '#token_types' => $token_types,
    ];

    $build['event__startdate'] = [
      '#title' => t('Event start date'),
      '#type' => 'textfield',
      '#default_value' => !is_null($config) ? $config->get('event.startdate') : $this->defaultConfiguration()['startdate'],
      '#description' => t("The string to be used for the event start date (this can include tokens)"),
      '#element_validate' => array('token_element_validate'),
      '#after_build' => array('token_element_validate'),
      '#token_types' => $token_types,
    ];

    $build['event__enddate'] = [
      '#title' => t('Event end date'),
      '#type' => 'textfield',
      '#default_value' => !is_null($config) ? $config->get('event.enddate') : $this->defaultConfiguration()['enddate'],
      '#description' => t("The string to be used for the event end date (this can include tokens)"),
      '#element_validate' => array('token_element_validate'),
      '#after_build' => array('token_element_validate'),
      '#token_types' => $token_types,
    ];

    $build['event__location'] = [
      '#title' => t('Event venue name'),
      '#type' => 'textfield',
      '#default_value' => !is_null($config) ? $config->get('event.location') : $this->defaultConfiguration()['location'],
      '#description' => t("The string to be used for the name of the event venue (this can include tokens)"),
      '#element_validate' => array('token_element_validate'),
      '#after_build' => array('token_element_validate'),
      '#token_types' => $token_types,
    ];

    $build['event__address'] = [
      '#title' => t('Event venue address'),
      '#type' => 'textfield',
      '#default_value' => !is_null($config) ? $config->get('event.address') : $this->defaultConfiguration()['address'],
      '#description' => t("The string to be used for the name of the event venue address (this can include tokens)"),
      '#element_validate' => array('token_element_validate'),
      '#after_build' => array('token_element_validate'),
      '#token_types' => $token_types,
    ];

    $build['event__image_url'] = [
      '#title' => t('Event image URL'),
      '#type' => 'textfield',
      '#default_value' => !is_null($config) ? $config->get('event.image_url') : $this->defaultConfiguration()['image_url'],
      '#description' => t("The string to be used for the image URL of the event (this can include tokens)"),
      '#element_validate' => array('token_element_validate'),
      '#after_build' => array('token_element_validate'),
      '#token_types' => $token_types,
    ];

    $build['event__description'] = [
      '#title' => t('Event description'),
      '#type' => 'textfield',
      '#default_value' => !is_null($config) ? $config->get('event.description') : $this->defaultConfiguration()['description'],
      '#description' => t("The string to be used for the description of the event (this can include tokens)"),
      '#element_validate' => array('token_element_validate'),
      '#after_build' => array('token_element_validate'),
      '#token_types' => $token_types,
    ];

    $build['event__offers'] = [
      '#title' => t('Event offer URL'),
      '#type' => 'textfield',
      '#default_value' => !is_null($config) ? $config->get('event.offers') : $this->defaultConfiguration()['offers'],
      '#description' => t("The string to be used for the URL where tickets can be purchased (this can include tokens and will default to the page url)"),
      '#element_validate' => array('token_element_validate'),
      '#after_build' => array('token_element_validate'),
      '#token_types' => $token_types,
    ];

    $build['event__token_help'] = array(
      '#theme' => 'token_tree_link',
      '#token_types' => $token_types,
    );

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'type' => [],
      'name' => '[node:title]',
      'startdate' => '',
      'enddate' => '',
      'location' => '',
      'address' => '',
      'image_url' => '',
      'description' => '',
      'offers' => '',
    ];
  }

}
