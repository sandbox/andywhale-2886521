<?php

namespace Drupal\structured_data\Plugin\StructuredDataType;

use Drupal\structured_data\EntityStructuredDataTypeBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'site links' structured data type.
 *
 * @StructuredDataType(
 *   id = "course",
 *   name = @Translation("Course"),
 *   type = "Course"
 * )
 */
class Course extends EntityStructuredDataTypeBase {
  
  public function getData() {
    return parent::getData() + $this->getCourseData();
  }

  protected function getCourseData() {
    $config = \Drupal::config('structured_data.settings');
    return [
      'name' => $this->tokenizeString($config->get('course.name')),
      'description' => $this->tokenizeString($config->get('course.description')),
      'provider' => [
        '@type' => 'Organization',
        'name' => \Drupal::config('system.site')->get('name'),
        'sameAs' => \Drupal::request()->getSchemeAndHttpHost(),
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state, $config) {
    $build = parent::buildConfigurationForm($form, $form_state, $config);

    $token_types = ['node'];
    
    $build['course__name'] = [
      '#title' => t('Course name'),
      '#type' => 'textfield',
      '#default_value' => !is_null($config) ? $config->get('course.name') : $this->defaultConfiguration()['name'],
      '#description' => t("The string to be used for the course name (this can include tokens)"),
      '#element_validate' => array('token_element_validate'),
      '#after_build' => array('token_element_validate'),
      '#token_types' => $token_types,
    ];

    $build['course__description'] = [
      '#title' => t('Course description'),
      '#type' => 'textfield',
      '#default_value' => !is_null($config) ? $config->get('course.description') : $this->defaultConfiguration()['description'],
      '#description' => t("The string to be used for the course description (this can include tokens)"),
      '#element_validate' => array('token_element_validate'),
      '#after_build' => array('token_element_validate'),
      '#token_types' => $token_types,
    ];

    $build['course__token_help'] = array(
      '#theme' => 'token_tree_link',
      '#token_types' => $token_types,
    );

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'type' => [],
      'name' => '[node:title]',
      'description' => '',
    ];
  }

}
