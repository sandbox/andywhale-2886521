<?php

namespace Drupal\structured_data\Plugin\StructuredDataType;

use Drupal\structured_data\StructuredDataTypeBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'social profile' structured data type.
 *
 * @StructuredDataType(
 *   id = "social_profile",
 *   name = @Translation("Social Profile"),
 *   type = "Organization"
 * )
 */
class SocialProfile extends StructuredDataTypeBase {
  
  public function getData() {
    return parent::getData() + $this->getSocialProfileData();
  }

  protected function getSocialProfileData() {
    return [
      'url' => \Drupal::request()->getSchemeAndHttpHost(),
      'sameAs' => $this->getSocialMediaUrls(),
    ];
  }

  protected function getSocialMediaUrls() {
    return preg_split('/[\s]+/', \Drupal::config('structured_data.settings')->get('social_profile.links'));
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state, $config) {
    $build = [];

    $build['social_profile__links'] = [
      '#title' => t('Social Profile URLs'),
      '#type' => 'textarea',
      '#multiple' => TRUE,
      '#default_value' => !is_null($config) ? $config->get('social_profile.links') : $this->defaultConfiguration()['links'],
      '#description' => t("The URL to each of your social media profiles (one per line)"),
    ];

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'links' => [],
    ];
  }

}