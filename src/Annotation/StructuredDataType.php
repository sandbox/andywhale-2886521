<?php

namespace Drupal\structured_data\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a data type of structured data.
 *
 * Plugin Namespace: Plugin\structured_data\StructuredDataType
 *
 * @see \Drupal\structured_data\StructuredDataManager
 * @see plugin_api
 *
 * @Annotation
 */
class StructuredDataType extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The name of the data type.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $name;

  /**
   * The json+ld type.
   *
   * @var string
   */
  public $type;

}
