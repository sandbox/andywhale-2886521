<?php

namespace Drupal\structured_data;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\structured_data\StructuredDataTypeBase;
use Drupal\structured_data\Exception\StructuredDataException;

class StructuredDataManager extends DefaultPluginManager {

  /**
   * @var Drupal\Core\Entity\EntityInterface
   */
  protected $entity;

  /**
   * Constructs an StructuredDataManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations,
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/StructuredDataType', $namespaces, $module_handler, 'Drupal\structured_data\DataTypeInterface', 'Drupal\structured_data\Annotation\StructuredDataType');
    $this->alterInfo('structured_data_type_info');
    $this->setCacheBackend($cache_backend, 'structured_data_types');
  }

  /**
   * {@inheritdoc}
   */
  public function getDataTypes() {
    $instances = $configuration = [];
    if ($this->hasEntity()) {
      $configuration['entity'] = $this->getEntity();
    }
    $settings = \Drupal::config('structured_data.settings');
    foreach ($this->getDefinitions() as $dataType) {
      if ($settings->get($dataType['id'] . '.enabled')) {
        try {
          $instance = $this->createInstance($dataType['id'], $configuration)->verifyPluginConfiguration();
          $instances[] = $instance;
        } catch (StructuredDataException $e) {
        }
      }
    }
    return $instances;
  }

  /**
   * {@inheritdoc}
   */
  public function getData() {
    $data = [];
    foreach ($this->getDataTypes() as $dataType) {
      try {
        $data = $this->addOrMergeData($dataType, $data);
      } catch (StructuredDataException $e) {
      }
    }
    return array_values($data);
  }

  protected function addOrMergeData(StructuredDataTypeBase $dataType, array $data) {
    if (!isset($data[$dataType->getType()])) {
      $data[$dataType->getType()] = [];
    }
    $data[$dataType->getType()] = array_merge($data[$dataType->getType()], $dataType->getData());
    return $data;
  }

  /**
   * Set the entity for the structured data manager.
   */
  public function setEntity(EntityInterface $entity) {
    $this->entity = $entity;
    return $this;
  }

  /**
   * Get the entity from the structured data manager.
   */
  public function getEntity() {
    return $this->entity;
  }

  /**
   * Does the structured data manager have an entity attached.
   */
  public function hasEntity() {
    return ($this->entity) ? true : false;
  }

}