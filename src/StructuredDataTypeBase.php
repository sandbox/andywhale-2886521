<?php

namespace Drupal\structured_data;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Component\Serialization\Json;

abstract class StructuredDataTypeBase extends PluginBase implements DataTypeInterface {

  public function verifyPluginConfiguration() {
    return $this;
  }

  public function getData() {
    return [
      '@context' => 'http://schema.org',
      '@type' => $this->getType(),
    ];
  }

  public function getId() {
    return $this->pluginDefinition['id'];
  }

  public function getName() {
    return $this->pluginDefinition['name'];
  }

  public function getType() {
    return $this->pluginDefinition['type'];
  }

}