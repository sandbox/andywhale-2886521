<?php

namespace Drupal\structured_data\Exception;

class StructuredDataException extends \Exception {
  
  /**
   * Constructs a StructuredDataException object.
   *
   * @param string $message
   *   The message for the exception.
   */
  public function __construct($message = NULL) {
    parent::__construct($message);
  }

}