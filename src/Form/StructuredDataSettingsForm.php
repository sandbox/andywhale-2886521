<?php

namespace Drupal\structured_data\Form;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\structured_data\StructuredDataManager;
use Drupal\structured_data\StructuredDataTypeBase;

/**
 * Class StructuredDataSettingsForm.
 *
 * @package Drupal\structured_data\Form
 */
class StructuredDataSettingsForm extends ConfigFormBase {

  protected $structuredDataManager;

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('plugin.manager.structured_data.types')
    );
  }

  public function __construct(ConfigFactoryInterface $configFactory, StructuredDataManager $structuredDataManager) {
    parent::__construct($configFactory);
    $this->structuredDataManager = $structuredDataManager;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'structured_data.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'structured_data_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $config = $this->config('structured_data.settings');

    $form['structured_data_plugins'] = [
      '#type' => 'fieldset',
      '#title' => 'Structured Data Plugins',
    ];

    $form['structured_data_config'] = [
      '#type' => 'vertical_tabs',
      '#title' => strtoupper('Structured Data Plugin Settings'),
    ];

    $plugins = $this->structuredDataManager->getDefinitions();
    ksort($plugins);

    foreach($plugins as $dataType) {
      $plugin = $this->structuredDataManager->createInstance($dataType['id']);
      $form[$plugin->getId() . '_status'] = [
        '#type' => 'checkbox',
        '#title' => $plugin->getName(),
        '#default_value' => ($config->get($plugin->getId() . '.enabled')) ?: 0,
        '#group' => 'structured_data_plugins',
      ];
      if (method_exists($plugin, 'buildConfigurationForm')) {
        $form[$plugin->getId()] = $plugin->buildConfigurationForm($form, $form_state, $config);
        $form[$plugin->getId()]['#type'] = 'details';
        $form[$plugin->getId()]['#title'] = $plugin->getName()->render();
        $form[$plugin->getId()]['#group'] = 'structured_data_config';
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $config = $this->config('structured_data.settings');

    foreach($this->structuredDataManager->getDefinitions() as $dataType) {
      $plugin = $this->structuredDataManager->createInstance($dataType['id']);
      $config->set($plugin->getId() . '.enabled', ($form_state->getValue($plugin->getId() . '_status')) ?: 0 );
      if (method_exists($plugin, 'buildConfigurationForm')) {
        $build = $plugin->buildConfigurationForm($form, $form_state, $config);
        foreach ($build as $field_id => $field) {
          $config_id = str_replace('__', '.', $field_id);
          $config->set($config_id, $this->getFilteredValue($form_state, $plugin, $field_id));
        }
      }
    }
    $config->save();
  }

  protected function getFilteredValue(FormStateInterface $form_state, StructuredDataTypeBase $plugin, $field_id) {
    $value = $form_state->getValue($field_id);
    return (is_array($value)) ? array_filter($value) : $value;
  }

}
