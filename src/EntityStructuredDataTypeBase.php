<?php

namespace Drupal\structured_data;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Entity\EntityInterface;
use Drupal\structured_data\StructuredDataTypeBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\structured_data\Exception\StructuredDataException;

abstract class EntityStructuredDataTypeBase extends StructuredDataTypeBase implements DataTypeInterface {

  public function verifyPluginConfiguration() {
    if (!isset($this->configuration['entity'])) {
      throw new StructuredDataException('No Entity specified');
    }
    if (!($this->configuration['entity'] instanceof EntityInterface)) {
      throw new StructuredDataException('No Entity specified');
    }
    $config = \Drupal::config('structured_data.settings');
    if (!in_array($this->configuration['entity']->bundle(), $config->get($this->getId() . '.type'))) {
      throw new StructuredDataException('Not a valid entity type for this plugin');
    }
    return parent::verifyPluginConfiguration();
  }

  /**
   * Tokenize a given string.
   */
  protected function tokenizeString($string) {
    return \Drupal::token()->replace($string, $this->getTokenData(), ['clear' => true]);
  }

  /**
   * Get a token array of the attached entity.
   */
  protected function getTokenData() {
    return [$this->configuration['entity']->getEntityTypeId() => $this->configuration['entity']];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state, $config) {
    $build = $options = [];
    $content_types = \Drupal::service('entity.manager')->getStorage('node_type')->loadMultiple();

    foreach ($content_types as $content_type) {
      $options[$content_type->id()] = $content_type->label();
    }

    $build[$this->getId() . '__type'] = [
      '#title' => t('Content Type(s)'),
      '#type' => 'checkboxes',
      '#default_value' => !is_null($config) ? $config->get($this->getId() . '.type') : $this->defaultConfiguration()['type'],
      '#description' => t("The content types that are available for this plugin type"),
      '#options' => $options,
    ];

    return $build;
  }

}