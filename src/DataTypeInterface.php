<?php

namespace Drupal\structured_data;

use Drupal\Component\Plugin\PluginInspectionInterface;

interface DataTypeInterface extends PluginInspectionInterface {
  
  public function getData();

}